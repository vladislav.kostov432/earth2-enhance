async function initMarketplace() {
    if (window.location.href.indexOf('#marketplace') !== -1 && !document.getElementById('marketEnhance')) {
        document.querySelector("#root > div > div.content-holder > div > div.settings-header > div").innerHTML += ` <button id="marketEnhance" class="btn is-green flex-right">Click to Enhance</button>`

        document.getElementById('marketEnhance').onclick = () => {
            document.querySelector(".marketplace").innerHTML = `<iframe src="https://earth2.market/allOffers" width="${window.innerWidth - 20}" height=${window.innerHeight - document.querySelector('.header').getBoundingClientRect().height - 20} ></iframe>`
            getXpathNodesArray('//a[contains(@href,"#")]').forEach(a=>a.onclick=_=>setTimeout(_=>window.location.reload(),10))
        }
    }

}


if (document.readyState == 'loading') {
    // still loading, wait for the event
} else {
    // DOM is ready!
    (async function f() {
        await awaitSelector("#root > div > div.content-holder > div > div.settings-header > div")
        setTimeout(initMarketplace,500)
    })()
}