async function initProfilePage() {
    if (window.location.href.indexOf('#profile') !== -1 && !document.getElementById('profileEnhance')) {
        document.querySelector("#root > div > div.content-holder > div > div.settings-header > div").innerHTML += ` <button id="profileEnhance" class="btn is-green flex-right">Click to Enhance</button>`
        let id = window.location.href.split('#profile/')[1].slice(0, 36);
        document.getElementById('profileEnhance').onclick = () => {
            document.querySelector('.profile.section').innerHTML = `<iframe src="https://www.earth2stats.xyz/profile/${id}/" width="${window.innerWidth - 20}" height=${window.innerHeight - document.querySelector('.header').getBoundingClientRect().height - 20} ></iframe>`
            getXpathNodesArray('//a[contains(@href,"#")]').forEach(a => a.onclick = _ => setTimeout(_ => window.location.reload(), 10))
        }
    }

}

if (document.readyState == 'loading') {
    // still loading, wait for the event
} else {
    // DOM is ready!
    (async function f() {
        await awaitSelector("#root > div > div.content-holder > div > div.settings-header > div")
        setTimeout(initProfilePage,500)
    })()
}