if (window.location.host==='www.earth2stats.xyz'){
    document.querySelector('nav').remove()
    document.querySelector('div.alert').remove()
}

if (window.location.host==='app.earth2.io'){
    document.querySelector('a[href="https://earth2.io"]').href='/'
}


//script by Mihaj and Gašper
(function() {
    'use strict';
    let intervalShort = 1000;
    let intervalLong = 5000; // when inside of the grid, so properties load propertly
    let interval = intervalShort;


    setInterval(() => {
        if(window.location.hash.includes("thegrid")){
            interval = intervalLong;
        }else{
            interval = intervalShort;
        }
        //console.log(`check: [${window.location.hash}]`);
        let keyToRemove = "MAP_STORE_CENTER";
        if(localStorage.getItem(keyToRemove) != null){
            console.log(`remove [${keyToRemove}]`);
            localStorage.removeItem(keyToRemove); // removes coordinates that couse the problem
        }
    }, interval);

    console.log("hash check added");

})();