if (!document.getElementById('lostBids')) {
    var isGettingOngoing, itemsPerPage = 100
    document.querySelector('div.main-menu .desktop').appendChild(createElementFromHTML(`<a id="lostBids" class="item waves-effect sidenav-close ">Active Bids</a>`))
    document.getElementById('lostBids').onclick = _ => {
        if (!isGettingOngoing) {
            isGettingOngoing = true
            getOngoingBids()
            document.querySelector("div.content-holder").innerHTML = `<h4>Please Wait while earth2 enhance is checking for ongoing bids</h4>
<h5>Links to properties will appear as soon as a bid is found</h5>
<h6>while you are waiting consider leaving a <a href="https://chrome.google.com/webstore/detail/earth2-enhance/heddplgeoodepomjkjhabdnoagihhdjj?hl=en&authuser=0" target="_blank">review in the chrome web store</a> or <a href="https://www.buymeacoffee.com/mcrdy455" target="_blank">Buying me a coffee</a></h6>
<div id="scanProgress" style="font-size: 22px">Calculating Progress...</div>
<div id="lostBidsList" style="text-align: center;"></div>
<strong>WARNING! Don't use this functionality more than once a day, this may result in earth2 not loading for 30-60 minutes! To stop the scan, just close this tab!</strong>
`
        }
    }
}

async function getAllBids() {
    return new Promise(async (resolve, reject) => {
        let req = fetch(`https://app.earth2.io/api/v2/my/balance_changes/?offset=0&limit=${itemsPerPage}&balance_change_type=BID_MADE`, {
            method: "get", mode: "cors",
            headers: {
                'Content-Type': 'application/json',
                'x-csrftoken': getCookie('csrftoken')
            }, credentials: 'same-origin'
        })
        let res = await (await req).json();
        console.log(res)
        let {count, results} = res
        let allBids = [...results];

        if (count > itemsPerPage) {
            for (let i = 1; i < Math.ceil(count / itemsPerPage); i++) {
                let {results} = await (await fetch(`https://app.earth2.io/api/v2/my/balance_changes/?offset=${i * itemsPerPage}&limit=${itemsPerPage}&balance_change_type=BID_MADE`, {
                    method: "get", mode: "cors",
                    headers: {
                        'Content-Type': 'application/json',
                        'x-csrftoken': getCookie('csrftoken')
                    }, credentials: 'same-origin'
                })).json();
                allBids.push(...results)
            }
        }
        resolve(allBids)
    })
}

async function checkForBids(propetyId) {
    let {token} = await getStorage('token')
    let username = document.querySelector('a[class="header__dropdown-btn btn"]').innerText.split('\n')[0].toUpperCase()
    let propData = await (await fetch('https://app.earth2.io/graphql', {
        method: "post", mode: "cors",
        headers: {
            'Content-Type': 'application/json',
            'x-csrftoken': token,
        }, credentials: 'same-origin',
        body: `{"query":"{getOngoingBids(landfieldId: \\"${propetyId}\\")   {result,buyer{username},offerSet {createdStr,value}}}"}`
    })).json()
    console.log(propData)
    let bid =  propData.data.getOngoingBids.find(b => b.result === "ONGOING" && b.buyer.username.toUpperCase() === username)
    bid.landfield = {id:propetyId}
    return bid
}

async function getOngoingBids() {
    let allBids = await getAllBids();
    let ongoing = []
    document.getElementById('scanProgress').innerHTML = `Progress: <strong id="scanDone">0</strong>/<span>${allBids.length}</span> bids checked, found <strong id="forgotCount">0</strong>  ongoing`
    for (let i = 0; i < allBids.length; i++) {
        console.log(allBids[i])
        try {
            let bid = await checkForBids(allBids[i].linked_object.id)
            document.getElementById('scanDone').innerText = i + 1;

            if (i > 20) {
                //to prevent earth2 from flagging you as spam
                await wait(1)
                if (i > 300) {
                    await wait(0.5)
                }
                if (i > 1000) {
                    await wait(0.5)
                }
            }

            if (bid) {
console.log(ongoing)
                let sameBid = ongoing.find(a => a.landfield.id === allBids[i].linked_object.id)
                if (!sameBid) {
                    ongoing.push(bid)
                    document.getElementById('forgotCount').innerText = ongoing.length
                    document.getElementById('lostBidsList').innerHTML += `
           <div style="display: inline-block;padding: 4px;border: solid 2px #4fe3c1"><span><img class="property-landfield-img" style="height: 70px; object-fit: cover" src="https://s3-ap-southeast-2.amazonaws.com/prod-app-media.earth2.io/thumbnails/${allBids[i].linked_object.id}.jpg"></span><br/><span>value: <strong>${bid.offerSet[0].value}</strong></span><br/><span>${bid.offerSet[0].createdStr}</span><br/><a href="https://app.earth2.io/#thegrid/${allBids[i].linked_object.id}" target="_blank">Click To Open</a></div>`
                }
            }
        } catch (e) {
            console.log(e)
        }
    }
    console.log(removeDuplicates(ongoing.filter(a => a).map(a => {
        return {id: a.landfield.id, value: a.offerSet[0].value, created: a.offerSet[0].createdStr}
    }), 'id'))
}
