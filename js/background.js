

multiBrowser.tabs.onUpdated.addListener(async (id, info, lsd) => {
    console.log()
    if (info.status === 'complete' && lsd.url.indexOf('app.earth2.io') !== -1) {
        await executeScript({file: 'js/utils.js'}, id)
        if (lsd.url.indexOf('#propertyInfo') !== -1) {
            await executeScript({file: 'js/propertyInfo/chartsLib.js'}, id)
            await executeScript({file: 'js/propertyInfo/index.js'}, id)
        }

        if (lsd.url.indexOf('#profile') !== -1) {
            await executeScript({file: 'js/utils.js'}, id)
            await executeScript({file: 'js/profile/index.js'}, id)
        }

        if (lsd.url.indexOf('#theGrid') !== -1) {
            await executeScript({file: 'js/theGrid/index.js'}, id)
        }

        if (lsd.url.indexOf('#marketplace') !== -1) {
            await executeScript({file: 'js/marketplace/index.js'}, id)
        }

        if (lsd.url.indexOf('#leaderboards') !== -1) {
            await executeScript({file: 'js/leaderboards/index.js'}, id)
        }


        await executeScript({file: "js/ownPages/lostBids.js"}, id)
    }
});

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


var options = [ "requestHeaders"];
var filter = {
    "urls": ["http://app.earth2.io/*", "https://app.earth2.io/*"]
};


multiBrowser.webRequest.onBeforeSendHeaders.addListener( (e)=>{
    let token = e.requestHeaders.find(a=>a.name==="X-CSRFToken")
    if (token)  setStorage({token:token.value})
}, filter, options);