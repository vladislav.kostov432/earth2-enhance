async function initPropertyInfoPage() {
    if (window.location.href.indexOf('#propertyInfo') !== -1) {

        {
            let id = (await awaitXpath('//div[@class="details-holder"]//div[contains(text(),"-")]')).innerText;

            //width per chart
            let availableWidth = (window.innerWidth - 40) / 3

            if (availableWidth > 250) {

                let section = document.querySelector('.section');
                //create empty canvases
                document.querySelectorAll('.content-holder.column')[0].innerHTML = `<div style="text-align: center;">
<div style="display: inline-block"><canvas id="transactions" width="320" height="180"></canvas></div>
<div style="display: inline-block"><canvas id="bids" width="320" height="180"></canvas></div>
<div style="display: inline-block"><canvas id="listings" width="320" height="180"></canvas></div><br/>
<span style="bottom: 10px; left:10px;   position: fixed;">  This Page have been modified by <strong>Earth2 Enhance</strong> Powered by <a href="https://earth2.market/" target="_blank">earth2.market</a> </span>
</div>`

                document.querySelectorAll('.content-holder.column')[0].style.maxWidth = '100vw'
                document.querySelectorAll('.content-holder.column')[0].style.margin = 0;
                {
                    //fetch Data
                    let propData = await (await fetch('https://app.earth2.io/graphql', {
                        method: "post", mode: "cors",
                        headers: {
                            'Content-Type': 'application/json',
                            'x-csrftoken':getCookie('csrftoken')
                        },   credentials: 'same-origin',
                        body: `{"query":"{getLandfieldDetail(landfieldId: \\"${id}\\") { tileClassRevenue,id,bidentrySet {id,result, offerSet {createdStr,value, user{username}}}, transactionSet { id , owner{id,username}, price,time,previousOwner{id,username}}, owner { id,username} }}"}`
                    })).json()


                    // declare data variables
                    let {data: {getLandfieldDetail: {transactionSet, bidentrySet, tileClassRevenue}}} = propData
                    transactionSet = transactionSet.reverse()
                    bidentrySet = bidentrySet.reverse()


                    //LIT
                    document.querySelector(".col-md-6 > .details-holder").innerHTML += `
                <div class="label">Generated Land Income</div>
                <div class="value"> <b>E$ ${tileClassRevenue.toFixed(5)} </b> (since ${timeSince(new Date(transactionSet[0].time))} ago) </div>
                `

                    //create transactions chart
                    let transctx = document.getElementById('transactions').getContext('2d');
                    let transactionsChart = new Chart(transctx, {
                        type: 'line',
                        data: {
                            labels: transactionSet.map(a => `From: ${a.previousOwner ? a.previousOwner.username.substr(0, 20) : "Earth2"}\nTo: ${a.owner.username.substr(0, 20)} \nFor: E$${a.price.toFixed(2)} (${timeSince(new Date(a.time))} ago)`),
                            datasets: [{
                                label: 'Transaction History',
                                data: transactionSet.map(a => a.price),
                                backgroundColor: 'rgba(80, 227, 194,0.3)',
                                borderColor: 'rgba(80, 227, 194,1)',
                                borderWidth: 1
                            }]
                        }, options: {
                            scales: {
                                xAxes: [{
                                    display: false //this will remove all the x-axis grid lines
                                }]
                            }
                        }
                    });
                    //set size (max width 500px and fixed height 250px)
                    transactionsChart.canvas.parentNode.style.height = '250px';
                    transactionsChart.canvas.parentNode.style.width = Math.min(500, availableWidth) + 'px';


                    //create bids charts
                    let bidctx = document.getElementById('bids').getContext('2d');
                    let bidChart = new Chart(bidctx, {
                        type: 'line',
                        data: {
                            labels: bidentrySet.map(bb => `${bb.offerSet[0].user.username} placed a bid for ${bb.offerSet[0].value}\nThe bid ${bb.result === "ONGOING" ? "is" : "was"} ${bb.result !== "SOLD" ? bb.result + "ED" : "Canceled, because the property was SOLD"}\n${bb.offerSet[0].createdStr}`),
                            datasets: [{
                                label: 'Bidding History',
                                data: bidentrySet.map(bb => bb.offerSet[0].value),
                                backgroundColor: 'rgba(80, 227, 194,0.3)',
                                borderColor: 'rgba(80, 227, 194,1)',
                                borderWidth: 1
                            }]
                        }, options: {
                            scales: {
                                xAxes: [{
                                    display: false //this will remove all the x-axis grid lines
                                }]
                            }
                        }
                    });
                    //set size (max width 500px and fixed height 250px)
                    bidChart.canvas.parentNode.style.height = '250px';
                    bidChart.canvas.parentNode.style.width = Math.min(500, availableWidth) + 'px';


                }
                {
                    //fetch data for market history price
                    let histctx = document.getElementById('listings').getContext('2d');
                    let {priceHistory} = await (await fetch('https://earth2.market/getPropertyPriceHistory?id=' + id)).json()
                    if (priceHistory.length === 1) priceHistory[1] = priceHistory[0]
                    let historyChart = new Chart(histctx, {
                        type: 'line',
                        data: {
                            labels: priceHistory.map(a => 'The property have been seen\nfor sale at earth2.market\nfor E$' + a.toFixed(2)),
                            datasets: [{
                                label: 'Listing Price History',
                                data: priceHistory,
                                backgroundColor: 'rgba(80, 227, 194,0.3)',
                                borderColor: 'rgba(80, 227, 194,1)',
                                borderWidth: 1
                            }]
                        }, options: {
                            scales: {
                                xAxes: [{
                                    display: false //this will remove all the x-axis grid lines
                                }]
                            }
                        }
                    });
                    historyChart.canvas.parentNode.style.height = '250px';
                    historyChart.canvas.parentNode.style.width = Math.min(500, availableWidth) + 'px';
                }


            }
        }
    }

}

if (document.readyState == 'loading') {

} else {
    // DOM is ready!
    setTimeout(initPropertyInfoPage,1000);
}