class EventObject {
    constructor() {
        this.view = window;
        this.bubbles = true;
        this.cancelable = true;
    }
}
let multiBrowser = (chrome || browser);

console.log('Extra Dev Tools injected to this tab')


function getStorage(opts = null) {
    return new Promise(resolve => multiBrowser.storage.local.get(opts ? opts.split(' ') : opts, (storage) => resolve(storage)))
}

function setStorage(changes) {
    return new Promise(resolve => multiBrowser.storage.local.set(changes, (d) => resolve(d)))
}

function sendMessage(message) {
    return new Promise((resolve, reject) => {
        try {
            multiBrowser.runtime.sendMessage(message, resolve);
        } catch (e) {
            reject(e)
        }
    })
}

function executeScript(details, tabId) {
    return new Promise((resolve, reject) => {
        try {
            if (tabId) multiBrowser.tabs.executeScript(tabId, details, resolve);
            else multiBrowser.tabs.executeScript(details, resolve);
        } catch (e) {
            reject(e)
        }
    })
}

function removeByIndex(array, index, count = 1) {
    array.splice(index, count);
    return array
}

function insertAtIndex(array,index,element0,element1,element2){
    for (let i = arguments.length;i>1;i--){
        array.splice(index, 0, arguments[i])
    }
    return array
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function copyStringToClipboard(string) {
    var el = document.createElement('textarea');
    el.value = string;
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function getXpathNodesArray(path) {
    let query = document.evaluate(path, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    return Array(query.snapshotLength).fill(0).map((e, index) => query.snapshotItem(index));
}

function getElementByXpath(path) {
    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

function getRelativeByXpath(target, xPath) {
    return document.evaluate('.' + xPath, target, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
}

function getRelativeArrayByXpath(target, xPath) {
    let query = document.evaluate('.' + xPath, target, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    return Array(query.snapshotLength).fill(0).map((e, index) => query.snapshotItem(index));
}

function elementIsWithinView(element) {
    //finds a post withing the view of the screen
    let elemPos = element.getBoundingClientRect().top;
    if (elemPos > 0 && elemPos < window.innerHeight) {
        return true
    }
}

function randomString(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function appendZeros(num, length, zeroChar = '0') {
    let str = num + '';
    while (str.length < length) str = zeroChar + str
    return str
}

function randomNum(min, max, float = false) {
    let weight = 1
    if (!float && float !== 0) {
        //todo auto detect weight
        // 0.1 0.2 => float = 2
        // 0.1 0.9 => float = 1
        // 0.001 0.01 => float = 3
        float = 0
        let minDec = Math.ceil(min) - min;
        let maxDec = Math.ceil(max) - max;
        if (minDec || maxDec) {
            let minLen = min.toString().replace('0.', '').length;
            let maxLen = max.toString().replace('0.', '').length;
            float = Math.max(minLen, maxLen)
        }
    }
    for (let i = 0; i < float; i++) {
        weight *= 10;
    }
    return Number((Math.floor(Math.random() * (max * weight - min * weight + 1)) / weight + min).toFixed(float || 0));
}

function randomNumArr(length = 10, min = 0, max = 100, toFixed = 0) {
    let arr = [];
    for (let i = 0; i < length; i++) {
        let num = Number(randomNum(min, max).toFixed(toFixed));
        arr.push(num)
    }
    return arr;
}

function awaitSelector(selector, maxTimeInSeconds = 30) {
    return new Promise((resolve, reject) => {
        let time = 0
        let checkInterval = setInterval(() => {
            time += 0.1
            let elem = document.querySelector(selector)
            if (elem) {
                clearInterval(checkInterval)
                resolve(elem)
            } else if (time > maxTimeInSeconds) {
                reject(`no element found for ${time} seconds`)
            }

        }, 100)
    })
}

function awaitXpath(xPath, maxTimeInSeconds = 30) {
    return new Promise((resolve, reject) => {
        let time = 0
        let checkInterval = setInterval(() => {
            time += 0.1
            let elem = getElementByXpath(xPath)
            if (elem) {
                clearInterval(checkInterval)
                resolve(elem)
            } else if (time > maxTimeInSeconds) {
                reject(`no element found for ${time} seconds`)
            }

        }, 100)
    })
}

function awaitRelative(target, xPath, maxTimeInSeconds = 30) {
    return new Promise((resolve, reject) => {
        let time = 0
        let checkInterval = setInterval(() => {
            time += 0.1
            let elem = getRelativeByXpath(target, xPath)
            if (elem) {
                clearInterval(checkInterval)
                resolve(elem)
            } else if (time > maxTimeInSeconds) {
                reject(`no element found for ${time} seconds`)
            }

        }, 100)
    })
}

function getElementWithinView(arrOfElems) {

    let elem = arrOfElems.find((el) => {
        return elementIsWithinView(el)
    });
    if (elem) return elem;
    else {
        document.scrollingElement.scrollTop += 100;
        return getElementWithinView(arrOfElems)
    }
}

function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    // Change this to div.childNodes to support multiple top-level nodes
    return div.firstChild;
}

function getNiceDate(date = new Date()) {
    return `${('0' + date.getDate()).substr(-2, 2)}.${('0' + (date.getMonth() + 1)).substr(-2, 2)}.${date.getFullYear().toString().substr(2, 2)}`;
}

function getNiceTime(date = new Date()) {
    return `${('0' + date.getDate()).substr(-2, 2)}.${('0' + (date.getMonth() + 1)).substr(-2, 2)}.${date.getFullYear().toString().substr(2, 2)}/${('0' + date.getHours()).substr(-2, 2)}:${('0' + date.getMinutes()).substr(-2, 2)}`;
}

function fakeClick(el) {
    el.dispatchEvent(new MouseEvent('mouseover', new EventObject()));
    el.dispatchEvent(new PointerEvent('pointerdown', new EventObject()));
    el.dispatchEvent(new MouseEvent('mousedown', new EventObject()));
    el.dispatchEvent(new PointerEvent('pointerup', new EventObject()));
    el.dispatchEvent(new MouseEvent('mouseup', new EventObject()))
    el.dispatchEvent(new MouseEvent('click', new EventObject()))
}

async function fakeInput(target) {
    return new Promise((resolve, reject) => {
        try {

            let keydown = {
                altKey: false,
                bubbles: true,
                cancelBubble: false,
                cancelable: true,
                composed: true,
                ctrlKey: false,
                currentTarget: null,
                defaultPrevented: false,
                isComposing: false,
                isTrusted: true,
                metaKey: false,
                repeat: false,
                returnValue: true,
                shiftKey: false,
                srcElement: target,
                target: target,
                type: "keydown",
                view: window
            }

            let keypress = {
                altKey: false,
                bubbles: true,
                cancelBubble: false,
                cancelable: true,
                composed: true,
                ctrlKey: false,
                currentTarget: null,
                defaultPrevented: false,
                isComposing: false,
                isTrusted: true,
                metaKey: false,
                repeat: false,
                returnValue: true,
                shiftKey: false,
                srcElement: target,
                target: target,
                type: "keypress",
                view: window
            }

            let keyup = {
                altKey: false,
                bubbles: true,
                cancelBubble: false,
                cancelable: true,
                composed: true,
                ctrlKey: false,
                currentTarget: null,
                defaultPrevented: false,
                isComposing: false,
                isTrusted: true,
                metaKey: false,
                repeat: false,
                returnValue: true,
                shiftKey: false,
                srcElement: target,
                target: target,
                type: "keyup",
                view: window,
                which: 96,
            }

            let inputEv = {
                bubbles: true,
                cancelBubble: false,
                cancelable: false,
                composed: true,
                currentTarget: null,
                dataTransfer: null,
                defaultPrevented: false,
                inputType: "insertText",
                isComposing: false,
                isTrusted: true,
                returnValue: true,
                sourceCapabilities: null,
                srcElement: target,
                target: target,
                type: "input",
                view: null,
            }

            let changeEv = {
                bubbles: true,
                cancelBubble: false,
                cancelable: false,
                composed: false,
                currentTarget: null,
                defaultPrevented: false,
                isTrusted: true,
                returnValue: true,
                srcElement: target,
                target: target,
                type: "change"
            }

            target.dispatchEvent(new KeyboardEvent('keydown', keydown));
            target.dispatchEvent(new KeyboardEvent('keypress', keypress));
            try {
                var textEvent = document.createEvent('TextEvent');
                textEvent.initTextEvent('textInput', true, true, null, '', 0, 'us-US')
                target.dispatchEvent(textEvent)
            } catch (e) {
                console.log('mozilla firefox $H!T')
            }
            try {
                var inputEvent = new InputEvent('input', inputEv)
                target.dispatchEvent(inputEvent)
            } catch (e) {

            }
            target.dispatchEvent(new Event('input', inputEv));
            target.dispatchEvent(new Event('change', changeEv));
            target.dispatchEvent(new KeyboardEvent('keyup', keyup));
            target.click()
            resolve()
        } catch (e) {
            reject(e)
        }
    })
}


function writeText(text, target) {
    var textEvent = document.createEvent('TextEvent');
    textEvent.initTextEvent('textInput', true, true, null, text, 0, 'us-US')
    target.dispatchEvent(textEvent)
    let inputEv = {
        bubbles: true,
        cancelBubble: false,
        cancelable: false,
        composed: true,
        currentTarget: null,
        dataTransfer: null,
        defaultPrevented: false,
        inputType: "insertText",
        isComposing: false,
        isTrusted: true,
        returnValue: true,
        sourceCapabilities: null,
        srcElement: target,
        target: target,
        type: "input",
        view: null,
    }
    var inputEvent = new InputEvent('input', inputEv)
    target.dispatchEvent(inputEvent)
}

function dispatchEnter(el) {
    let eventObj = document.createEvent("Events");
    eventObj.initEvent("keydown", true, true);
    eventObj.which = 13;
    eventObj.keyCode = 13;
    el.dispatchEvent(eventObj);
    console.log(eventObj)
}

function fakeMouseOver(target) {
    let mouseOver = {
        altKey: false,
        bubbles: true,
        button: 0,
        buttons: 0,
        cancelBubble: false,
        cancelable: true,
        composed: true,
        ctrlKey: false,
        currentTarget: null,
        defaultPrevented: false,
        detail: 0,
        eventPhase: 0,
        isTrusted: true,
        layerX: 161,
        layerY: 34,
        metaKey: false,
        movementX: 0,
        movementY: 0,
        offsetX: 161,
        offsetY: 34,
        pageX: 489,
        pageY: 8749,
        returnValue: true,
        shiftKey: false,
        srcElement: target,
        target: target,
        type: "mouseover",
        view: window
    }


    target.dispatchEvent(new MouseEvent('mouseover', mouseOver));
    target.dispatchEvent(new MouseEvent('pointerenter', mouseOver));
    target.dispatchEvent(new MouseEvent('pointerover', mouseOver));
}

function removeDuplicates(myArr, prop) {
    if (!prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.indexOf(obj) === pos;
        });
    } else return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

function leaveDuplicates(myArr, prop) {
    if (!prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.indexOf(obj) !== pos;
        });
    } else return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) !== pos;
    });
}

function isHidden(el) {
    return (el.offsetParent === null)
}


function findParentBySelector(elm, selector) {

    function collectionHas(a, b) { //helper function (see below)
        for (var i = 0, len = a.length; i < len; i++) {
            if (a[i] == b) return true;
        }
        return false;
    }


    var all = document.querySelectorAll(selector);
    var cur = elm.parentNode;
    while (cur && !collectionHas(all, cur)) { //keep going up until you find a match
        cur = cur.parentNode; //go up
    }
    return cur; //will return null if not found
}

function findParentByClass(elm, className) {
    while (elm !== document.body) {
        elm = elm.parentNode
        console.log(elm.classList)
        if (elm.classList.contains(className)) return elm
    }
    return null
}

function wait(sec) {
    return new Promise((r, j) => setTimeout(r, sec * 1000))
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function rWait(min = 2, max = 4) {
    return wait(randomNum(min, max))
}


function clearAllIntervals() {
    var interval_id = window.setInterval("", 9999); // Get a reference to the last
                                                    // interval +1
    for (var i = 1; i < interval_id; i++) window.clearInterval(i);
//for clearing all intervals
}


function transliterate(word) {
    var answer = ""
        , a = {};

    a["Ё"] = "YO";
    a["Й"] = "I";
    a["Ц"] = "TS";
    a["У"] = "U";
    a["К"] = "K";
    a["Е"] = "E";
    a["Н"] = "N";
    a["Г"] = "G";
    a["Ш"] = "SH";
    a["Щ"] = "SHT";
    a["З"] = "Z";
    a["Х"] = "H";
    a["Ъ"] = "a";
    a["ё"] = "yo";
    a["й"] = "i";
    a["ц"] = "ts";
    a["у"] = "u";
    a["к"] = "k";
    a["е"] = "e";
    a["н"] = "n";
    a["г"] = "g";
    a["ш"] = "sh";
    a["щ"] = "sht";
    a["з"] = "z";
    a["х"] = "h";
    a["ъ"] = "a";
    a["Ф"] = "F";
    a["Ы"] = "I";
    a["В"] = "V";
    a["А"] = "a";
    a["П"] = "P";
    a["Р"] = "R";
    a["О"] = "O";
    a["Л"] = "L";
    a["Д"] = "D";
    a["Ж"] = "ZH";
    a["Э"] = "E";
    a["ф"] = "f";
    a["ы"] = "i";
    a["в"] = "v";
    a["а"] = "a";
    a["п"] = "p";
    a["р"] = "r";
    a["о"] = "o";
    a["л"] = "l";
    a["д"] = "d";
    a["ж"] = "zh";
    a["э"] = "e";
    a["Я"] = "Ya";
    a["Ч"] = "CH";
    a["С"] = "S";
    a["М"] = "M";
    a["И"] = "I";
    a["Т"] = "T";
    a["Ь"] = "io";
    a["Б"] = "B";
    a["Ю"] = "YU";
    a["я"] = "ya";
    a["ч"] = "ch";
    a["с"] = "s";
    a["м"] = "m";
    a["и"] = "i";
    a["т"] = "t";
    a["ь"] = "i";
    a["б"] = "b";
    a["ю"] = "yu";
    a[' '] = '_';
    for (let i in word) {
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined) {
                answer += word[i];
            } else {
                answer += a[word[i]];
            }
        }
    }
    return answer.toLowerCase();
}

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}


function shuffle(array) {
    var tmp, current, top = array.length;
    if (top) while (--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
    }
    return array;
}

function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
        return Math.floor(interval) + " years";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
        return Math.floor(interval) + " months";
    }
    interval = seconds / 86400;
    if (interval > 1) {
        return Math.floor(interval) + " days";
    }
    interval = seconds / 3600;
    if (interval > 1) {
        return Math.floor(interval) + " hours";
    }
    interval = seconds / 60;
    if (interval > 1) {
        return Math.floor(interval) + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}
var aDay = 24*60*60*1000;
console.log(timeSince(new Date(Date.now()-aDay)));
console.log(timeSince(new Date(Date.now()-aDay*2)));


/**  extensions only */

function requestCaptcha(captchaImage, textInput) {
    return new Promise(async (resolve, reject) => {
        let imgCaptcha = getBase64Image(captchaImage);
        await sendMessage({imgCaptcha})

        let interv = setInterval(async () => {
            let {solution} = await getStorage('solution')
            if (solution) {
                textInput.value = solution
                clearInterval(interv)
                resolve(solution)
            }
        }, 5000)


    })
}
function solveCaptcha(base64) {
    return new Promise((resolve, reject) => {

        fetch("https://api.anti-captcha.com/createTask", {
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            },
            "body": JSON.stringify({
                "clientKey": "1e225a650c1e3422972ce719056f17a5",
                "task": {
                    "type": "ImageToTextTask",
                    "body": base64,
                    "phrase": false,
                    "case": false,
                    "numeric": 0,
                    "math": 0,
                    "minLength": 0,
                    "maxLength": 0
                }
            })
        }).then(response => {
            console.log(response);
            return response.json()
        }).then(wtf => {
            setTimeout(() => getAnswer(wtf), 5000)
        });

        function getAnswer(wtf) {
            console.log(wtf);
            fetch('https://api.anti-captcha.com/getTaskResult', {
                method: "POST", "headers": {
                    "content-type": "application/json"
                },
                "body": JSON.stringify({
                    "clientKey": "c837fd80940c4bfa5fdf136cbc6b68e0",
                    "taskId": wtf.taskId
                })
            }).then(res => res.json()).then(j => {
                console.log(j)
                if (j.status === 'processing') setTimeout(() => getAnswer(wtf), 3000);
                else if (j.status === 'ready') {
                    let sol = j.solution.text;
                    resolve(sol)
                }
            })
        }
    })
}

function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to guess the
    // original format, but be aware the using "image/jpg" will re-encode the image.
    var dataURL = canvas.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function clearBrowser(minutesAgo) {
    return new Promise((resolve) => {
        var someTimeAgo = (new Date()).getTime() - minutesAgo*60*1000;
        multiBrowser.browsingData.remove({
            "since": someTimeAgo
        }, {
            "cache": true,
            "cookies": true,
            "downloads": true,
            "formData": true,
            "history": true,
            "indexedDB": true,
            "localStorage": true,
            "pluginData": true,
            "passwords": true,
            "serviceWorkers": true
        }, resolve);
    })
}
let closeAllWindows = (withHost = false) => multiBrowser.windows.getAll({}, wins => {
    wins.forEach((a, ind) => {
        if (ind || withHost) multiBrowser.windows.remove(a.id)
    })
})